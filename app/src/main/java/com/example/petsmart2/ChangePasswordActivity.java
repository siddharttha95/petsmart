package com.example.petsmart2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangePasswordActivity extends AppCompatActivity {


    ImageButton btnreturnChangePassword;
    EditText previousPassword;
    EditText newPassword;
    EditText confirmPassword;
    Button btnchangeyourpassword;
    ProgressDialog progressDialog;
    private Activity activity;
    public Context context;
    private SharedPreferences prefs;
    final String DOMAIN_URL = "http://app-cd1904d0-4bca-4cda-ad7c-6d66028168f5.cleverapps.io/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changepassword_activity);

        btnreturnChangePassword = findViewById(R.id.imageButton_botonreturnChangepassword2);
        btnchangeyourpassword = findViewById(R.id.button_changeyourpassword2);
        previousPassword = findViewById(R.id.editText_previouspassword2);
        newPassword = findViewById(R.id.editText_newpassword2);
        confirmPassword = findViewById(R.id.editText_confirmpassword2);

        activity = ChangePasswordActivity.this;
        context = this.getApplicationContext();
        prefs = PreferenceManager.getDefaultSharedPreferences(context);

        btnreturnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChangePasswordActivity.this,SettingActivity.class);
                startActivity(intent);
            }
        });

        btnchangeyourpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog = new ProgressDialog(activity);
                progressDialog.setMessage("Logging In...");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(true);
                progressDialog.show();

                String previous_password = previousPassword.getText().toString();
                String password = newPassword.getText().toString();
                String confirm_Password = confirmPassword.getText().toString();
                changePassword(previous_password, password, confirm_Password);
            }
        });
    }

    public void changePassword (String previousPassword, String newPassword, String confirmPassword){

        String userid = prefs.getString("id", "");
        String url = DOMAIN_URL + "/reset_password.php?previousPassword="+previousPassword+"&newPassword="+newPassword+"&confirmPassword="+confirmPassword+"&userid="+userid;

        progressDialog.dismiss();

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String resp) {
                final String response = resp;
                JSONObject mainObject = null;

                try {

                    mainObject = new JSONObject(response);
                    final Boolean success = mainObject.getBoolean("success");

                    if(success){
                        Intent intent = new Intent(ChangePasswordActivity.this, SettingActivity.class);
                        startActivity(intent);
                    }else{
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(context,"Wrong Form", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                final String error = err.getMessage();
                progressDialog.dismiss();
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        Volley.newRequestQueue(this).add(request);
    }

}
