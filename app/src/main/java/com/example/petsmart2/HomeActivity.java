package com.example.petsmart2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.example.petsmart2.ui.login.LoginActivity;

public class HomeActivity extends AppCompatActivity {

    ImageButton btnAlarm;
    ImageButton btnAlert;
    ImageButton btnConnection;
    ImageButton btnRecord;
    //ImageButton btnCamera;
    ImageButton btnSetting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        btnAlarm = findViewById(R.id.imageButton_alarm1);
        btnAlert = findViewById(R.id.imageButton_alerts1);
        btnConnection = findViewById(R.id.imageButton_conexion1);
       // btnCamera = findViewById(R.id.imageButton_camera);
        btnRecord = findViewById(R.id.imageButton_record1);
        btnSetting = findViewById(R.id.imageButton_setting1);

        btnAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, AlarmActivity.class);
                startActivity(intent);
            }
        });

        btnAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, AlertActivity.class);
                startActivity(intent);
            }
        });
        btnConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ConnectionsActivity.class);
                startActivity(intent);
            }
        });
        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, RecordsActivity.class);
                startActivity(intent);
            }
        });
        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, SettingActivity.class);
                startActivity(intent);
            }
        });

    }
}
