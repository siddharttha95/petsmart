package com.example.petsmart2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.petsmart2.ui.login.LoginActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class Register extends AppCompatActivity {

    Button btnEndRegister;

    EditText etEmail;
    EditText etPassword;
    EditText etPetName;
    Spinner spinnerKindOfPet;
    Spinner spinnerAge;
    Spinner spinnerSex;
    Spinner spinnerRaza;
    Spinner spinnerFoodRegister;
    private Activity activity;
    public Context context;
    ProgressDialog progressDialog;
    private SharedPreferences prefs;
    final String DOMAIN_URL = "http://app-cd1904d0-4bca-4cda-ad7c-6d66028168f5.cleverapps.io/";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnEndRegister = findViewById(R.id.button_EndR);
        spinnerKindOfPet = findViewById(R.id.spinner_kp);
        spinnerAge = findViewById(R.id.spinner_age);
        spinnerSex = findViewById(R.id.spinner_petsex);
        spinnerRaza = findViewById(R.id.spinner_raza);
        spinnerFoodRegister = findViewById(R.id.spinner_food1);
        etEmail = findViewById(R.id.editText_email);
        etPassword = findViewById(R.id.editText_password);
        etPetName = findViewById(R.id.editText_pname);

        activity = Register.this;
        context = this.getApplicationContext();
        prefs = PreferenceManager.getDefaultSharedPreferences(context);

        // Agregar elementos a spinners:
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.pet_types, android.R.layout.simple_spinner_dropdown_item);
        spinnerKindOfPet.setAdapter(adapter);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.pet_age, android.R.layout.simple_spinner_dropdown_item);
        spinnerAge.setAdapter(adapter2);
        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(this, R.array.pet_sex, android.R.layout.simple_spinner_dropdown_item);
        spinnerSex.setAdapter(adapter3);
        ArrayAdapter<CharSequence> adapter4 = ArrayAdapter.createFromResource(this, R.array.pet_raza, android.R.layout.simple_spinner_dropdown_item);
        spinnerRaza.setAdapter(adapter4);
        ArrayAdapter<CharSequence> adapter5 = ArrayAdapter.createFromResource(this, R.array.pet_foodregister, android.R.layout.simple_spinner_dropdown_item);
        spinnerFoodRegister.setAdapter(adapter5);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        btnEndRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog = new ProgressDialog(activity);
                progressDialog.setMessage("Logging In...");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(true);
                progressDialog.show();

                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                String petName = etPetName.getText().toString();
                String kindOfPet = spinnerKindOfPet.getSelectedItem().toString();
                String age = spinnerAge.getSelectedItem().toString();
                String sex = spinnerSex.getSelectedItem().toString();
                String raza = spinnerAge.getSelectedItem().toString();
                String food = spinnerFoodRegister.getSelectedItem().toString();

                register(email,password,petName,kindOfPet,age,sex,raza,food);
            }
        });
    }

    public void register(String email, String password, String petName, String kindOfPet, String age, String sex, String raza, String food){

        String url = DOMAIN_URL + "/set_user.php?email="+email+"&password="+password+"&petName="+petName
                +"&kindOfPet="+kindOfPet+"&age="+age+"&sex="+sex+"&raza="+raza+"&food="+food;

        progressDialog.dismiss();

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String resp) {
                final String response = resp;
                JSONObject mainObject = null;

                try {

                    mainObject = new JSONObject(response);
                    final Boolean success = mainObject.getBoolean("success");

                    if(success){

                        // Obtener usuario de JSON
                        JSONObject user = mainObject.getJSONObject("user");

                        String id = user.getString("id");
                        String email = user.getString("email");
                        String password = user.getString("password");

                        prefs.edit().putString("id", id).commit();
                        prefs.edit().putString("email", email).commit();
                        prefs.edit().putString("password", password).commit();

                        Intent intent = new Intent(Register.this, HomeActivity.class);
                        startActivity(intent);
                    }else{
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(context,"Wrong Form", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                final String error = err.getMessage();
                progressDialog.dismiss();
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        Volley.newRequestQueue(this).add(request);
    }

}
