package com.example.petsmart2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class SoundActivity extends AppCompatActivity {

    ImageButton btnreturnSound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sound1_activity);

        btnreturnSound = findViewById(R.id.imageButton_botonreturnsound1);

        btnreturnSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SoundActivity.this, SettingActivity.class);
                startActivity(intent);
            }
        });
    }
}
