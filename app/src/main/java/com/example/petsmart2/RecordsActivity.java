package com.example.petsmart2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class RecordsActivity extends AppCompatActivity {

    ImageButton btnreturnRecord1;
    ImageButton btnCompletedTask;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.records_activity);

        btnreturnRecord1= findViewById(R.id.imageButton_botonreturnrecords);
        btnCompletedTask= findViewById(R.id.imageButton_completedtask4);

        btnreturnRecord1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecordsActivity.this,HomeActivity .class);
                startActivity(intent);
            }
        });

        btnCompletedTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecordsActivity.this,CompletedTaskActivity .class);
                startActivity(intent);
            }
        });
    }
}
