package com.example.petsmart2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.example.petsmart2.ui.login.LoginActivity;

public class UserAccountActivity extends AppCompatActivity {

    ImageButton btnreturnUsers;
    ImageButton btnChangePassword;
    ImageButton btnLogOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.useracount_activity);

        btnreturnUsers = findViewById(R.id.imageButton_botonreturnuser1);
        btnChangePassword = findViewById(R.id.imageButton_changepassword2);
        btnLogOut = findViewById(R.id.imageButton_logout1);


        btnreturnUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserAccountActivity.this, SettingActivity.class);
                startActivity(intent);
            }
        });

        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserAccountActivity.this,ChangePasswordActivity .class);
                startActivity(intent);
            }
        });
        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserAccountActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
