package com.example.petsmart2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class SupportActivity extends AppCompatActivity {

    ImageButton btnreturnSupport;
    Button btnSendMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.support_activity);

        btnreturnSupport = findViewById(R.id.imageButton_botonreturnsupport2);
        btnSendMessage = findViewById(R.id.button_sendmessage);


        btnreturnSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SupportActivity.this, SettingActivity.class);
                startActivity(intent);
            }
        });

        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SupportActivity.this, SettingActivity.class);
                startActivity(intent);
            }
        });



    }
}
