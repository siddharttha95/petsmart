package com.example.petsmart2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.petsmart2.ui.login.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class SnackActivity extends AppCompatActivity {

    ImageButton btnreturnSnack;
    Button btnDateSnack;
    Button btnTimeSnack;
    Spinner spinnerCantidadSnack;
    Button btnSaveSnackAlarm;
    private static final String CERO = "0";
    private static final String GUION = "-";
    private static final String DOS_PUNTOS = ":";
    //Variables para obtener la fecha
    int mes, dia, anio, hora, minuto;
    EditText etDate;
    EditText etTime;
    ProgressDialog progressDialog;
    private Activity activity;
    public Context context;
    private SharedPreferences prefs;
    final String DOMAIN_URL = "http://app-cd1904d0-4bca-4cda-ad7c-6d66028168f5.cleverapps.io/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.snack_activity);

        btnreturnSnack = findViewById(R.id.imageButton_snackreturn1);
        btnDateSnack = findViewById(R.id.button_FechaSnackalarm2);
        btnTimeSnack= findViewById(R.id.button_TimeSnackalarm);
        spinnerCantidadSnack = findViewById(R.id.spinner_cantidadsnack2);
        btnSaveSnackAlarm = findViewById(R.id.button_saveSnackalarm);
        etDate = findViewById(R.id.editText_FechaSnackAlarm);
        etTime = findViewById(R.id.editText_TimeSnackAlarm);

        //Calendario para obtener fecha & hora
        Calendar c = Calendar.getInstance();

        //Variables para obtener la fecha
        mes = c.get(Calendar.MONTH);
        dia = c.get(Calendar.DAY_OF_MONTH);
        anio = c.get(Calendar.YEAR);
        hora = c.get(Calendar.HOUR_OF_DAY);
        minuto = c.get(Calendar.MINUTE);

        activity = SnackActivity.this;
        context = this.getApplicationContext();
        prefs = PreferenceManager.getDefaultSharedPreferences(context);

        btnDateSnack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerFecha();
            }
        });

        btnTimeSnack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerHora();
            }
        });

        btnreturnSnack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SnackActivity.this, AlarmActivity.class);
                startActivity(intent);
            }
        });

        btnSaveSnackAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog = new ProgressDialog(activity);
                progressDialog.setMessage("Logging In...");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(true);
                progressDialog.show();

                String date = etDate.getText().toString();
                String time = etTime.getText().toString();
                snack(date, time);
            }
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.pet_snackcantidad, android.R.layout.simple_spinner_dropdown_item);
        spinnerCantidadSnack.setAdapter(adapter);
    }

    private void obtenerFecha(){
        DatePickerDialog recogerFecha = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //Esta variable lo que realiza es aumentar en uno el mes ya que comienza desde 0 = enero
                final int mesActual = month + 1;
                //Formateo el día obtenido: antepone el 0 si son menores de 10
                String diaFormateado = (dayOfMonth < 10)? CERO + String.valueOf(dayOfMonth):String.valueOf(dayOfMonth);
                //Formateo el mes obtenido: antepone el 0 si son menores de 10
                String mesFormateado = (mesActual < 10)? CERO + String.valueOf(mesActual):String.valueOf(mesActual);
                //Muestro la fecha con el formato deseado
                etDate.setText(year + GUION + mesFormateado + GUION + diaFormateado);


            }
            //Estos valores deben ir en ese orden, de lo contrario no mostrara la fecha actual
            /**
             *También puede cargar los valores que usted desee
             */
        },anio, mes, dia);
        //Muestro el widget
        recogerFecha.show();

    }

    private void obtenerHora(){
        TimePickerDialog recogerHora = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //Formateo el hora obtenido: antepone el 0 si son menores de 10
                String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);

                //Muestro la hora con el formato deseado
                etTime.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + ":00");
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, hora, minuto, false);

        recogerHora.show();
    }

    public void snack(String date, String time){

        String userid = prefs.getString("id", "");
        String url = DOMAIN_URL + "/set_alarm.php?date="+date+"&time="+time+"&isFood=0&isSnack=1&userid="+userid;

        progressDialog.dismiss();

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String resp) {
                final String response = resp;
                JSONObject mainObject = null;

                try {

                    mainObject = new JSONObject(response);
                    Boolean success = mainObject.getBoolean("success");

                    if(success){
                        Intent intent = new Intent(SnackActivity.this, AlarmActivity.class);
                        startActivity(intent);
                    } else {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(context,"Wrong Form", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                final String error = err.getMessage();
                progressDialog.dismiss();
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        Volley.newRequestQueue(this).add(request);
    }

}
