package com.example.petsmart2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class ConnectionsActivity extends AppCompatActivity {
    ImageButton btnreturnConexion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connections_activity);

        btnreturnConexion= findViewById(R.id.imageButton_botonreturnconnections);

        btnreturnConexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConnectionsActivity.this,HomeActivity .class);
                startActivity(intent);
            }
        });

    }
}
