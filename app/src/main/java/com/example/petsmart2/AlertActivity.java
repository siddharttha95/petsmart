package com.example.petsmart2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.petsmart2.ui.login.LoginActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class AlertActivity extends AppCompatActivity {

    ImageButton btnreturnAlert;
    private Activity activity;
    public Context context;
    ProgressDialog progressDialog;
    private SharedPreferences prefs;
    final String DOMAIN_URL = "http://app-cd1904d0-4bca-4cda-ad7c-6d66028168f5.cleverapps.io/";
    ListView listAlerts;

    //LIST OF ARRAY STRINGS WHICH WILL SERVE AS LIST ITEMS
    ArrayList<String> listItems=new ArrayList<String>();

    //DEFINING A STRING ADAPTER WHICH WILL HANDLE THE DATA OF THE LISTVIEW
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alert_activity);

        btnreturnAlert= findViewById(R.id.imageButton_botonreturnalert1);

        listAlerts = findViewById(R.id.listview_Alerts1);

        activity = AlertActivity.this;
        context = this.getApplicationContext();
        prefs = PreferenceManager.getDefaultSharedPreferences(context);

        adapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listItems);
        listAlerts.setAdapter(adapter);

        getAlerts();

        btnreturnAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AlertActivity.this,HomeActivity .class);
                startActivity(intent);
            }
        });
    }
    public void getAlerts(){

        String user_id = prefs.getString("id", "");
        String url = DOMAIN_URL + "/get_alerts.php?user="+user_id;

        //progressDialog.dismiss();

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONArray jsonArray = new JSONArray(response);

                    // limpiar lista de listview antes de agregar
                    listItems.clear();

                    for (int i=0; i < jsonArray.length(); i++) {

                        //se obtiene objeto
                        final JSONObject object = jsonArray.getJSONObject(i);

                        // se agrega fecha a la lista
                        String datetime = object.getString("description_datatime");
                        String alert = object.getString("description_alert");
                        listItems.add(datetime+" ("+alert+")");

                    }
                    // esto actualiza la lista con los elementos que tenga "listItems"
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                final String error = err.getMessage();
                //progressDialog.dismiss();
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        Volley.newRequestQueue(this).add(request);
    }


}
